using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class JumpComponent : FindComponent
{
    
    [SerializeField] private int _jumpNumFlip;
    [SerializeField] private float _jumpDuraction;
    [SerializeField] private float _jumpPower;


    public override void DoEffect(Action callback)
    {
        transform.DOJump(transform.localPosition,_jumpPower, _jumpNumFlip, _jumpDuraction).OnComplete(() =>
        {
            gameObject.SetActive(false);
            callback?.Invoke();
        });
    }
}
