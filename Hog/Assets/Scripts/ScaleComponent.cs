using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class ScaleComponent : FindComponent
{
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _scaleDureation;

    public override void DoEffect(Action callback)
    {
        transform.DOScale(transform.localScale * _scaleMultiplier, _scaleDureation).OnComplete(() =>
        {
            gameObject.SetActive(false);
            callback?.Invoke();
        });

      
    }
}
