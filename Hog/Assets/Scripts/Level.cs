using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    private List<GameItem> _items = new List<GameItem>();
    private int _itemsCount;

    private void Awake()
    {
        Initialize();
    }

    public void Initialize()
    {
        GameItem[] gameItems = GetComponentsInChildren<GameItem>();

        _items.AddRange(gameItems);
        _itemsCount = _items.Count;

        for (int i = 0; i < _items.Count; i++)
        {
            _items[i].OnFind += OnItemFinded;
        }
    }

    private void OnItemFinded()
    {
        _itemsCount--;
        if (_itemsCount == 0)
        {
            Debug.Log("Level Complited");
        }
    }

}
