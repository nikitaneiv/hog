using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

[RequireComponent(typeof(SpriteRenderer))]
public class GameItem : MonoBehaviour
{

    private FindComponent _findComponent;
    private SpriteRenderer _spriteRenderer;

    public event Action OnFind;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _findComponent = GetComponent<FindComponent>();
    }

    private void OnMouseUpAsButton()
    {
        OnFindMethod();
    }

    public void OnFindMethod()
    {
        _findComponent.DoEffect(() => 
        {
            OnFind?.Invoke();
        });
        
    }

}
